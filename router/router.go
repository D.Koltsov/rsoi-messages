package router

import (
	"gitlab.com/D.Koltsov/rsoi-messages/controllers"

	"github.com/gin-gonic/gin"
)

func Initialize(r *gin.Engine) {
	r.GET("/", controllers.APIEndpoints)

	api := r.Group("")
	{

		api.GET("/messages", controllers.GetMessages)
		api.GET("/messages/:id", controllers.GetMessage)
		api.POST("/messages", controllers.CreateMessage)
		api.PUT("/messages/:id", controllers.UpdateMessage)
		api.DELETE("/messages/:id", controllers.DeleteMessage)

	}
}
