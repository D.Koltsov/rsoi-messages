package models

import "time"

type Message struct {
	ID        uint       `gorm:"primary_key;AUTO_INCREMENT" json:"id" form:"id"`
	Text      string     `json:"text" form:"text"`
	ChatID    uint       `json:"chat_id" form:"chat_id"`
	SenderID  uint       `json:"sender_id" form:"sender_id"`
	CreatedAt *time.Time `json:"created_at" form:"created_at"`
	UpdatedAt *time.Time `json:"updated_at" form:"updated_at"`
}
