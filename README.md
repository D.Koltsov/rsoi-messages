# API Server

Simple Rest API using gin(framework) & gorm(orm)

## Endpoint list

### Messages Resource

```
GET    /messages
GET    /messages/:id
POST   /messages
PUT    /messages/:id
DELETE /messages/:id
```

server runs at http://localhost:8080
