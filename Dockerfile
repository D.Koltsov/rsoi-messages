FROM dkoltsov/go-sqlite3:0.0.2
WORKDIR /go/src/gitlab.com/D.Koltsov/rsoi-messages
COPY . .
RUN go build -o='/go/bin/entrypoint' gitlab.com/D.Koltsov/rsoi-messages
EXPOSE 8080
ENTRYPOINT [ "/go/bin/entrypoint" ]